
class Material {
    constructor(price, square){
        this.price = price;
        this.square = square;
    }
}


class Wallpapper extends Material {
    constructor(width, length, price){
        super(price, width*length)
        this.width = width;
        this.length = length;
    }
}

class Laminate extends Material {
    constructor(length, width, amount, price){
    super(price, length*width);
    this.length = length;
    this.width = width;
    this.amount = amount;
    }
}


class Room {
    constructor(title, width, length, height){
        this.title = title;
        this.width = width;
        this.length = length;
        this.height = height;
    }
    stickWallpaper(wallpaper){
        const p = ((this.width + this.length)*2);
        const amountWidth = p / wallpaper.width;
        const amount = amountWidth * (this.length / wallpaper.length);
        const total = amount * wallpaper.price;
        this.wallpaperTotal = total;
    }
    putFloor(laminate){
        const s = this.width * this.length;
        const amountItems = s / laminate.square;
        const amount = amountItems / laminate.amount;
        const total = amount * laminate.price;
        this.floorTotal = total;
    }
    getRoomEstimate(){
        return this.floorTotal + this.wallpaperTotal;
    }
}

class Flat {
    constructor(){
        this.rooms = [];
    }
    addRoom(room){
        this.rooms.push(room);
    }
    getEstimate(){
        let tempSum = 0;
        for(let i = 0; i < this.rooms.length; i++){
           tempSum = tempSum + this.rooms[i].getRoomEstimate();
        }
        return `Итого: ${Math.round(tempSum)} сом`;
    }
}



const flat = new Flat();

const bedRoom = new Room('Спальня', 500, 600, 240);
const kitchen = new Room('Кухня', 300, 400, 240);
const livingRoom = new Room('Зал', 600, 800, 240);
flat.addRoom(bedRoom);
flat.addRoom(kitchen);
flat.addRoom(livingRoom);

const laminate = new Laminate(50, 15, 6, 600);
const wallpaper = new Wallpapper(100, 120, 1000);

bedRoom.stickWallpaper(wallpaper);
bedRoom.putFloor(laminate);

kitchen.stickWallpaper(wallpaper);
kitchen.putFloor(laminate);

livingRoom.stickWallpaper(wallpaper);
livingRoom.putFloor(laminate);

const estimate = flat.getEstimate();

console.log(estimate);
